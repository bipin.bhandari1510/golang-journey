package main

import (
	"net/http"

	"github.com/b33pin/myPak/pkg/handlers"
)

func main() {
	http.HandleFunc("/", handlers.Home)
	http.ListenAndServe(":8080", nil)

}

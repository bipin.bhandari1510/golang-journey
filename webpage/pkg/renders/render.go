package renders

import (
	"html/template"
	"net/http"
)

func RenderTemplate(w http.ResponseWriter, path string) {
	parsedTemplate, _ := template.ParseFiles("./templates/" + path)
	_ = parsedTemplate.Execute(w, nil)
	return
}

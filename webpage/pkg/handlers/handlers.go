package handlers

import (
	"net/http"

	"github.com/b33pin/myPak/pkg/renders"
)

func Home(rw http.ResponseWriter, r *http.Request) {
	renders.RenderTemplate(rw, "home_page.htm")
}

package main

type decks []string

func (d decks) print() {
	for i, card := range d {
		println(i, card)
	}
}
